/* eslint-env node, mocha */
/* eslint "import/no-extraneous-dependencies": ["error", {"devDependencies": true }] */
/* eslint-disable no-unused-expressions */
/* eslint-disable max-statements */

import React from 'react';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';
import chai from 'chai';

import fixtures from '../example/fixtures';
import Hello from '../src';

chai.should();

describe('Example test', () => {

    it('example', () => {
        true.should.be.true;
    });

});

describe('Hello', () => {

    let data;

    beforeEach(() => {
        data = Object.assign({}, fixtures);
    });

    it('renders an H1', () => {
        const wrapper = shallow(<Hello {...data}/>);
        wrapper.is('h1').should.be.true;
    });

    it('prints "hello 5 worlds!"', () => {
        const wrapper = mount(<Hello {...data}/>);
        chai.expect(wrapper.find('h1').text()).to.contain('Hello World (click here!!)');
    });

    it('on click on worlds calls to prop click', () => {
        data.click = sinon.spy();
        const wrapper = mount(<Hello {...data}/>);

        wrapper.find('a').simulate('click');
        data.click.called.should.be.true;
    });

});
