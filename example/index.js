/* eslint "import/no-extraneous-dependencies": ["error", {"devDependencies": true }] */
import React from 'react';
import ReactDOM from 'react-dom';
import Hello from '../src/index-with-styles';

// Example of use of the component in an application
import data from './fixtures';

ReactDOM.render(
    <Hello {...data}/>,
    document.getElementById('content')
);
