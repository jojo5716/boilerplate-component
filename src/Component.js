import { Component } from 'react';
import PropTypes from 'prop-types';

import template from './Component.template.jsx';

export default class Hello extends Component {


    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.click();
    }

    render() {
        const props = {
            ...this.props,
            onClick: this.handleClick
        };
        return template(props);
    }

}

Hello.propTypes = {
    hello: PropTypes.string.isRequired,
    number: PropTypes.number.isRequired,
    worlds: PropTypes.string.isRequired,
    click: PropTypes.func.isRequired
};

Hello.defaultProps = {
    hello: 'ho',
    number: 0,
    worlds: 'worl',
    click: () => { /* This comment prevents SQ from raising a major code smell error */ }
};
