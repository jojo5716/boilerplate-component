// Use this file in development envs only.
// Intended to generate a dist build with webpack or to execute the example.
// Do not use as an entry point or interface to outisde code.

import './styles/index.less';

import Component from './index';

export default Component;
