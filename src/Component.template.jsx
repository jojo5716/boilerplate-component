import React from 'react';


export default (props) => {
    const className = 'test';
    return (
        <h1 className={className}>
            {props.hello}&nbsp;
            {props.world}&nbsp;
            <a onClick={props.onClick}>(click here!!)</a>
        </h1>
    );
};
